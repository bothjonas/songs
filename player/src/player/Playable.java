package player;

public interface Playable {
	public void play();
	public void stop();
	public void pause();
}
